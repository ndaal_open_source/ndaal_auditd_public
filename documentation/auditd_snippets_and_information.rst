******************************************************************************
Log Management - Linux - auditd Rules, snippets and information
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>
   
.. contents:: Content
   :depth: 3

.. _Section_auditd_snippets_and_information:
   
auditd
------------------------------------------------------------------------------

.. figure:: _static/auditd_architecture.png
   :alt: auditd - Architecture Overview
   :width: 50%
   :align: center

   auditd - Architecture Overview

The **auditd** package contains a number of binaries that are used to manage 
the ``auditd daemon`` and search and analyze the recorded events:

- **auditctl**: a tool for the current configuration of the auditd daemon, 
  checking the status and running Rules
- **audispd**: daemon is the multiplication of logged events to other log 
  aggregation programs
- **aureport**: a tool for quick reports generated on the basis of a log 
  (``auditd.log``)
- **ausearch**: tool to search events from the log (``auditd.log``)
- **autrace**: a tool for analyzing the interaction of programs with the 
  kernel
- **aulast**: tool with the last command functionality but using log 
  ``auditd.log``
- **aulastlog**: tool with the functionality of the lastlog command but 
  using log ``auditd.log``
- **ausyscall**: mapping the system call ID to the name
- **auvirt**: auditing information on virtual machines

Installation
##############################################################################

In order to deploy **auditd** on **Debian**, you first ensure the 
``auditd daemon``, and the audit dispatching framework are deployed on the 
system:


.. Code:: bash
   
   sudo apt-get install auditd audispd-plugins

.. Note:: 
   Run the following command to load the **new configured auditd Rules**:

   ``sudo auditctl -R /etc/audit/audit.rules``


.. _Section_auditd_snippets_and_information_auditd.conf:

/etc/audit/plugins.d/syslog.conf
##############################################################################

We redirect **auditd** to **syslog**.

Therefore we have to update ``/etc/audit/plugins.d/syslog.conf``: 

.. Code:: bash
   
   # This file controls the configuration of the syslog plugin.
   # It simply takes events and writes them to syslog. The
   # arguments provided can be the default priority that you
   # want the events written with. And optionally, you can give
   # a second argument indicating the facility that you want events
   # logged to. Valid options are LOG_LOCAL0 through 7, LOG_AUTH,
   # LOG_AUTHPRIV, LOG_DAEMON, LOG_SYSLOG, and LOG_USER.

   active = yes
   direction = out
   path = /sbin/audisp-syslog
   type = always 
   args = LOG_INFO
   format = string

/etc/audit/auditd.conf
##############################################################################

Before you can actually start generating audit logs and processing them, 
configure the ``auditd daemon`` itself. The ``/etc/audit/auditd.conf`` 
configuration file determines how the audit system functions when the daemon 
has been started. [117]_ 

.. Code:: bash
   
   log_file = /var/log/audit/audit.log
   log_format = RAW
   log_group = root
   priority_boost = 4
   flush = INCREMENTAL
   freq = 20
   num_logs = 5
   disp_qos = lossy
   dispatcher = /sbin/audispd
   name_format = NONE
   ##name = mydomain
   max_log_file = 6
   max_log_file_action = ROTATE
   space_left = 75
   space_left_action = SYSLOG
   action_mail_acct = root
   admin_space_left = 50
   admin_space_left_action = SUSPEND
   disk_full_action = SUSPEND
   disk_error_action = SUSPEND
   ##tcp_listen_port =
   tcp_listen_queue = 5
   tcp_max_per_addr = 1
   ##tcp_client_ports = 1024-65535
   tcp_client_max_idle = 0
   cp_client_max_idle = 0

We enable and configure logging within Linux with ``auditd`` and ``audit.rules`` 
files [87]_ ; [88]_ ; [89]_ ; [90]_ ; [91]_ ; [92]_ ; [93]_ ; [94]_ ; [95]_ ; [96]_ ; [116]_ ;.

.. _Section_auditd_snippets_and_information_audit.rules:

audit.rules
##############################################################################

We are starting with the following initial ``audit.rules file`` (placed in the
repo under ``../documentation/source/_auditd/_audit.rules`` with file name 
``audit_initial.rules``) :

.....

.. literalinclude::
   ../_auditd/_audit.rules/audit_best_practices.rules
   :language: none
   :encoding: latin-1
   :linenos:
   :lines: 1-500
   :caption: audit_best_practices.rules part I

.. literalinclude::
   ../_auditd/_audit.rules/audit_best_practices.rules
   :language: none
   :encoding: latin-1
   :linenos:
   :lines: 501-1000
   :caption: audit_best_practices.rules part II

.. literalinclude::
   ../_auditd/_audit.rules/audit_best_practices.rules
   :language: none
   :encoding: latin-1
   :linenos:
   :lines: 1001-1500
   :caption: audit_best_practices.rules part III

.. literalinclude::
   ../_auditd/_audit.rules/audit_best_practices.rules
   :language: none
   :encoding: latin-1
   :linenos:
   :lines: 1501-2000
   :caption: audit_best_practices.rules part IV

.. literalinclude::
   ../_auditd/_audit.rules/audit_best_practices.rules
   :language: none
   :encoding: latin-1
   :linenos:
   :lines: 2001-2500
   :caption: audit_best_practices.rules part V

......

.. Hint::
   Best Practice Auditd Configuration by ndaal is available here [118]_ 

.....

.. Note:: 
   Further ``audit.rules`` file available in the repo under 
   ``../documentation/source/_auditd/_audit.rules``

.....

.. Hint:: 
   If you need a **Linux Auditd Rule set** mapped to 
   **MITRE's Attack Framework** than use ``auditd-attack.rules`` 
   instead. [116]_ 

   .. figure:: _static/mitre-attack-enterprise.png
      :alt: MITRE's Attack Framework Matrix
      :width: 75%
      :align: center

      MITRE's Attack Framework Matrix


References for Linux auditd Rules 
------------------------------------------------------------------------------

Based on **auditd Rules** published here:

- Gov.uk auditd Rules [109]_ 
- CentOS 7 hardening [110]_ 
- Linux audit repo [111]_
- Auditd high performance linux auditing [112]_
- For PCI DSS compliance see [113]_
- For NISPOM compliance see [114]_
- Best Practice Auditd Configuration by Neo23x0 [115]_
- Red Hat RHEL 7 Audit Rules and Controls [93]_ 
- SUSE Introduction [94]_ 
- SUSE Audit Scenarios [95]_ 
- Red Hat RHEL 8 Auditing the System [96]_ 
- MITRE's Attack Framework Auditd Configuration by [116]_
- Best Practice Auditd Configuration by ndaal is available here [118]_ 

Linux Syslog within Kubernetes
------------------------------------------------------------------------------

Here we havse some collected hints for usage of auditd within Kubernetes 
respective within Container.

- Kubernetes auditing [123]_ 

Kubernetes on Azure Cloud called AKS
##############################################################################

Common links for Kubernetes, Container and auditd on Azure 
are listed here:

- Azure Kubernetes Service (AKS) documentation portal  [119]_ 
- Azure aks-auditd provides an easy and highly configurable way to gain 
  visibility into your AKS worker node and container kernel level activity. 
  It does this by using auditd and audit rules mounted in a Daemonset as 
  ConfigMaps. [120]_ 

Kubernetes on Google Cloud called GKE
##############################################################################

Common links for Kubernetes, Container and auditd on Google Cloud 
are listed here:

- Google Kubernetes Engine (GKE) documentation portal [122]_ 
- Enabling Linux auditd logs on GKE nodes [121]_ 
- Audit Logging Policy for Google Kubernetes Engine (GKE) [125]_ 

Kubernetes on AWS Cloud called EKS
##############################################################################

Common links for Kubernetes, Container and auditd on AWS Cloud 
are listed here:

- Kubernetes on AWS documentation portal [124]_ 
-  

Linux Syslog Datasets
------------------------------------------------------------------------------

.. Seealso:: 
   :ref:`Here we some examples of collected Linux Syslog Datasets: <Section_Datasets_Mentioned_in_the_Introduction>`


.....

.. Rubric:: Footnotes

.. [1]
   InfluxDB
   https://docs.influxdata.com/InfluxD/v2.0/

.. [2]
   Telegraf
   https://docs.influxdata.com/Telegraf

.. [3]
   Grafana
   https://grafana.com/docs

.. [4]
   SciPy
   https://www.SciPy.org

.. [5]
   HAProxy
   http://www.haproxy.org

.. [6]
   https://letsencrypt.org

.. [7]
   https://www.haproxy.com/blog/lets-encrypt-acme2-for-haproxy/

.. [8]
   Debian Linux
   https://www.debian.org

.. [9]
   https://www.pulumi.com

.. [10]
   https://aws.amazon.com/de/free/?trk=ps_a134p000003yhZSAAY&trkCampaign=acq_paid_search_brand&sc_channel=ps&sc_campaign=acquisition_DACH&sc_publisher=google&sc_category=core&sc_country=DACH&sc_geo=EMEA&sc_outcome=Acquisition&sc_detail=aws%20trial&sc_content=Account_e&sc_matchtype=e&sc_segment=456911459016&sc_medium=ACQ-P|PS-GO|Brand|Desktop|SU|AWS|Core|DACH|EN|Text&s_kwcid=AL!4422!3!456911459016!e!!g!!aws%20trial&ef_id=EAIaIQobChMIhYbHvPbE8QIVQQGLCh3weQIKEAAYASAAEgKFm_D_BwE:G:s&s_kwcid=AL!4422!3!456911459016!e!!g!!aws%20trial&all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all#:~:text=Mehr%20entdecken-,Kostenloses%20Kontingent%20f%C3%BCr%20AWS,-%C3%9Cbersicht

.. [11]
   https://github.com/ovh/debian-cis

.. [12]
   https://www.Sphinx-doc.org

.. [13]
   https://docutils.sourceforge.io/docs/user/rst/quickref.html

.. [14]
   https://www.linode.com/docs/guides/how-to-configure-automated-security-updates-debian/

.. [15]
   https://pypi.org/project/pylint/

.. [16]
   https://realpython.com/python-pep8/

.. [17]
   https://pypi.org/project/safety/

.. [18]
   https://pypi.org/project/bandit/

.. [19]
   https://stackabuse.com/checking-vulnerabilities-in-your-python-code-with-bandit

.. [20]
   https://www.jenkins.io

.. [21]
   https://aws.amazon.com/de/vpc

.. [22]
   https://gitlab.com

.. [23]
   https://jfrog.com/artifactory

.. [24]
   https://www.fail2ban.org

.. [25]
   StevenBlack
   https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

.. [26]
   StevenBlack
   https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

.. [27]
   StevenBlack
   https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn-social/hosts

.. [28]
   WindowsSpyBlocker
   https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra.txt

.. [29]
   WindowsSpyBlocker
   https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra_v6.txt

.. [30]
   WindowsSpyBlocker
   https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt

.. [31]
   WindowsSpyBlocker
   https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy_v6.txt

.. [32]
   WindowsSpyBlocker
   https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update.txt

.. [33]
   WindowsSpyBlocker
   https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update_v6.txt

.. [34]
   This hosts file is brought to you by Dan Pollock and can be found at
   https://someonewhocares.org/hosts/zero/hosts

.. [35]
   pool.ntp.org
   https://www.pool.ntp.org/zone/de

.. [36]
   pool.ntp.org
   https://www.pool.ntp.org/zone/europe

.. [37]
   https://gitlab.com/vPierre/dns_hosts_updates

.. [38]
   https://apache.org

.. [39]
   https://en.wikipedia.org/wiki/Network_Time_Protocol

.. [40]
   https://gitlab.com/vPierre/ndaal_sicherheitskonzept

.. [41]
   https://en.wikipedia.org/wiki/Forward_secrecy

.. [42]
   https://google.github.io/styleguide/shellguide.html

.. [43]
   https://www.shellcheck.net

.. [44]
   https://stackoverflow.com/questions/38834434/how-are-127-0-0-1-0-0-0-0-and-localhost-different

.. [45]
   https://www.cfg2html.com

.. [46]
   https://github.com/konstruktoid/ansible-role-hardening
   
.. [47]
   https://www.clamav.net

.. [48]
   https://en.wikipedia.org/wiki/Malware

.. [49]
   https://iplists.firehol.org

.. [50]
   https://lite.ip2location.com/ip-address-ranges-by-country
   
.. [51]
   https://en.wikipedia.org/wiki/Kerckhoffs%27s_principle

.. [52]
   https://en.wikipedia.org/wiki/Claude_Shannon

.. [53]
   https://github.com/Scribery/tlog

.. [54]
   https://github.com/Scribery/ansible-tlog

.. [55]  
   https://www.sonarqube.org

.. [56] 
   https://github.com/lean-delivery/ansible-role-sonarqube

.. [57]
   https://github.com/weareinteractive/ansible-apache2

.. [58]
   https://github.com/ansible-lockdown/APACHE-2.4-STIG

.. [59]
   https://github.com/dev-sec/ansible-collection-hardening/tree/master/roles/ssh_hardening

.. [60]
   https://en.wikipedia.org/wiki/Private_network

.. [61]
   https://github.com/lean-delivery/ansible-role-jenkins

.. [62]
   https://github.com/monitoring-tools/telegraf-plugins

.. [63]
   https://github.com/monitoring-tools/telegraf-plugins/tree/master/iostat

.. [64]
   https://github.com/monitoring-tools/telegraf-plugins/tree/master/netstat

.. [65]
   https://github.com/monitoring-tools/telegraf-plugins/tree/master/top

.. [66]
   https://github.com/monitoring-tools/telegraf-plugins/tree/master/fd

.. [67]
   https://github.com/monitoring-tools/telegraf-plugins/tree/master/netspeed

.. [68]
   https://www.influxdata.com/influxdb-templates/fail2ban

.. [69]
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/fail2ban

.. [70]
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/bind

.. [71]
   https://github.com/bertvv/ansible-role-bind

.. [72]
   Telegraf PlugIn ethtool
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/ethtool

.. [73]
   Telegraf PlugIn InfluxDB
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/influxdb

.. [74]
   Telegraf PlugIn kernel_vmstat
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/kernel_vmstat

.. [75]
   Telegraf PlugIn ntpq
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/ntpq

.. [76]
   Telegraf PlugIn processes
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/processes

.. [77]
   Telegraf PlugIn swap
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/swap

.. [78]
   Telegraf PlugIn sysstat
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/sysstat

.. [79]
   Telegraf PlugIn tail
   https://github.com/influxdata/telegraf/tree/release-1.17/plugins/inputs/tail

.. [80]
   texlive
   http://www.tug.org/texlive

.. [81]
   texlive-full
   https://packages.debian.org/de/bullseye/texlive-full

.. [82]
   https://github.com/geerlingguy/ansible-role-certbot

.. [83]
   https://github.com/thefinn93/ansible-letsencrypt

.. [84]
   https://github.com/geerlingguy/ansible-for-devops

.. [85]
   https://docs.influxdata.com/influxdb/v2.0/backup-restore

.. [86]
   rsyncd.conf
   https://download.samba.org/pub/rsync/rsyncd.conf.html

.. [87]
   Gov.uk auditd Rules
   https://github.com/gds-operations/puppet-auditd/pull/1

.. [88]
   CentOS 7 hardening
   https://highon.coffee/blog/security-harden-centos-7/#auditd---audit-daemon

.. [89]   
   Linux audit repo
   https://github.com/linux-audit/audit-userspace/tree/master/rules

.. [90]
   Auditd high performance linux auditing
   https://linux-audit.com/tuning-auditd-high-performance-linux-auditing/

.. [91]
   For PCI DSS compliance
   https://github.com/linux-audit/audit-userspace/blob/master/rules/30-pci-dss-v31.rules

.. [92]
   For NISPOM compliance
   https://github.com/linux-audit/audit-userspace/blob/master/rules/30-nispom.rules

.. [93]
   Red Hat RHEL 7 Audit Rules and Controls
   https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-defining_audit_rules_and_control

.. [94]
   SUSE Introduction
   https://documentation.suse.com/sles/12-SP4/html/SLES-all/cha-audit-comp.html 

.. [95]
   SUSE Audit Scenarios
   https://documentation.suse.com/sles/11-SP4/html/SLES-all/cha-audit-scenarios.html

.. [96]
   Red Hat RHEL 8 Auditing the System
   https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/auditing-the-system_security-hardening

.. [97]
   https://owasp.org/www-project-top-ten

.. [98]
   https://scikit-learn.org

.. [99]
   https://templates.cloudonaut.io/en/stable

.. [100]
   https://github.com/juju4/ansible-harden-apache

.. [101]
   https://github.com/influxdata/influxdb-client-python#installation

.. [102]
   https://suricata.io

.. [103]
   https://github.com/ORTSOC/ansible-suricata

.. [104]
   https://github.com/influxdata/telegraf/tree/master/plugins/inputs/suricata

.. [105]
   https://www.influxdata.com/blog/network-security-monitoring-with-suricata-and-telegraf

.. [106]
   https://github.com/influxdata/telegraf/blob/release-1.14/plugins/inputs/syslog/README.md

.. [107]
   https://www.influxdata.com/integration/syslog

.. [108]
   https://www.cisecurity.org/benchmark/debian_linux

.. [109]
   Gov.uk auditd Rules
	https://github.com/gds-operations/puppet-auditd/pull/1

.. [110]
   CentOS 7 hardening
	https://highon.coffee/blog/security-harden-centos-7/#auditd---audit-daemon

.. [111]
   Linux audit repo
	https://github.com/linux-audit/audit-userspace/tree/master/rules

.. [112]
   Auditd high performance linux auditing
	https://linux-audit.com/tuning-auditd-high-performance-linux-auditing/

.. [113]
   For PCI DSS compliance
	https://github.com/linux-audit/audit-userspace/blob/master/rules/30-pci-dss-v31.rules

.. [114]
   For NISPOM compliance
	https://github.com/linux-audit/audit-userspace/blob/master/rules/30-nispom.rules

.. [115]
   Best Practice Auditd Configuration by Neo23x0
   https://github.com/Neo23x0/auditd

.. [116]
   A Linux Auditd configuration mapped to MITRE's Attack Framework
   https://github.com/bfuzzy/auditd-attack/blob/master/auditd-attack.rules

.. [117]
   auditd.conf - audit daemon configuration file
   https://manpages.debian.org/testing/auditd/auditd.conf.5.en.html

.. [118]
   Best Practice Auditd Configuration by ndaal
   https://gitlab.com/ndaal_open_source/ndaal_auditd_public/-/tree/main/ndaal

.. [119]
   Azure Kubernetes Service (AKS) documentation portal
   https://learn.microsoft.com/en-us/azure/aks/

.. [120]
   Azure aks-auditd
   https://github.com/Azure/aks-auditd

.. [121]
   Enabling Linux auditd logs on GKE nodes
   https://cloud.google.com/kubernetes-engine/docs/how-to/linux-auditd-logging

.. [122]
   Google Kubernetes Engine (GKE) 
   https://cloud.google.com/kubernetes-engine/docs/how-to/linux-auditd-logging

.. [123]
   Kubernetes auditing
   https://kubernetes.io/docs/tasks/debug/debug-cluster/audit/

.. [124]
   Kubernetes on AWS
   https://aws.amazon.com/kubernetes/

.. [125]
   Audit Logging Policy for Google Kubernetes Engine (GKE)
   https://cloud.google.com/kubernetes-engine/docs/concepts/audit-policy

