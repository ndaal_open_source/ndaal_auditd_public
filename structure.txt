.
├── CODE_OF_CONDUCT.md
├── CODE_OF_CONDUCT.txt
├── CODE_OF_CONDUCT_DE.md
├── CODE_OF_CONDUCT_DE.txt
├── LICENSE
├── Neo23x0
│   └── audit.rules
├── README.md
├── Vulnerability_Disclosure_Policy.md
├── audit-userspace
│   └── rules
│       ├── 10-base-config.rules
│       ├── 10-no-audit.rules
│       ├── 11-loginuid.rules
│       ├── 12-cont-fail.rules
│       ├── 12-ignore-error.rules
│       ├── 20-dont-audit.rules
│       ├── 21-no32bit.rules
│       ├── 22-ignore-chrony.rules
│       ├── 23-ignore-filesystems.rules
│       ├── 30-nispom.rules
│       ├── 30-ospp-v42-1-create-failed.rules
│       ├── 30-ospp-v42-1-create-success.rules
│       ├── 30-ospp-v42-2-modify-failed.rules
│       ├── 30-ospp-v42-2-modify-success.rules
│       ├── 30-ospp-v42-3-access-failed.rules
│       ├── 30-ospp-v42-3-access-success.rules
│       ├── 30-ospp-v42-4-delete-failed.rules
│       ├── 30-ospp-v42-4-delete-success.rules
│       ├── 30-ospp-v42-5-perm-change-failed.rules
│       ├── 30-ospp-v42-5-perm-change-success.rules
│       ├── 30-ospp-v42-6-owner-change-failed.rules
│       ├── 30-ospp-v42-6-owner-change-success.rules
│       ├── 30-ospp-v42.rules
│       ├── 30-pci-dss-v31.rules
│       ├── 30-stig.rules
│       ├── 31-privileged.rules
│       ├── 32-power-abuse.rules
│       ├── 40-local.rules
│       ├── 41-containers.rules
│       ├── 42-injection.rules
│       ├── 43-module-load.rules
│       ├── 44-installers.rules
│       ├── 70-einval.rules
│       ├── 71-networking.rules
│       ├── 99-finalize.rules
│       ├── Makefile.am
│       └── README-rules
├── auditd-attack
│   ├── LICENSE
│   ├── README.md
│   ├── attack_map.png
│   ├── auditd-attack.rules
│   ├── base_config.rules
│   └── layer-2.json
├── content_long_list.txt
├── content_summary.txt
├── create.sh
├── create.sh.sha3-512
├── dataset
│   └── audit_best_practices.rules
├── documentation
│   ├── auditd_snippets_and_information.rst
│   ├── ndaal
│   │   └── placeholder.txt
│   └── robots.txt
├── example
│   ├── 10-base-config.rules
│   ├── 10-no-audit.rules
│   ├── 11-loginuid.rules
│   ├── 12-cont-fail.rules
│   ├── 12-ignore-error.rules
│   ├── 20-dont-audit.rules
│   ├── 21-no32bit.rules
│   ├── 22-ignore-chrony.rules
│   ├── 23-ignore-filesystems.rules
│   ├── 30-ospp-v42-1-create-failed.rules
│   ├── 30-ospp-v42-1-create-success.rules
│   ├── 30-ospp-v42-2-modify-failed.rules
│   ├── 30-ospp-v42-2-modify-success.rules
│   ├── 30-ospp-v42-3-access-failed.rules
│   ├── 30-ospp-v42-3-access-success.rules
│   ├── 30-ospp-v42-4-delete-failed.rules
│   ├── 30-ospp-v42-4-delete-success.rules
│   ├── 30-ospp-v42-5-perm-change-failed.rules
│   ├── 30-ospp-v42-5-perm-change-success.rules
│   ├── 30-ospp-v42-6-owner-change-failed.rules
│   ├── 30-ospp-v42-6-owner-change-success.rules
│   ├── 30-ospp-v42.rules
│   ├── 30-pci-dss-v31.rules
│   ├── 30-stig.rules
│   ├── 31-privileged.rules
│   ├── 32-power-abuse.rules
│   ├── 40-local.rules
│   ├── 41-containers.rules
│   ├── 42-injection.rules
│   ├── 43-module-load.rules
│   ├── 44-installers.rules
│   ├── 70-einval.rules
│   ├── 71-networking.rules
│   ├── 99-finalize.rules
│   ├── README-rules
│   └── audit.rules
├── ndaal
│   ├── LICENSE
│   └── audit_best_practices.rules
├── robots.txt
├── security.txt
├── structure.txt
└── tools
    ├── checkin_auditd.sh
    └── checkin_auditd.sh.sha3-512

11 directories, 102 files
