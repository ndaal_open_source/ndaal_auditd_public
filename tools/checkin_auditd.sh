#!/usr/bin/env bash
# Author: Pierre Gronau <Pierre.Gronau@ndaal.eu>
# Copyright 2024
# License: All content is licensed under the terms of the <MIT License>
# Developed on: Debian 12.x; macOS Sequoia x86 architecture
# Tested on: Debian 12.x; macOS Sequoia x86 architecture
#
# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# This is equivalent to set -e. It causes the script to exit
# immediately if any command exits with a non-zero status.
#
# Exit on error inside any functions or subshells.
set -o errtrace
# This setting ensures that the ERR trap is inherited by shell functions,
# command substitutions, and commands executed in a subshell environment.
#
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# This is equivalent to set -u. It treats unset variables as
# an error when substituting.
#
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# This setting causes a pipeline to return the exit status of the last command
# in the pipe that returned a non-zero status.
#
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
# nosemgrep: ifs-tampering
IFS=$'\n\t'

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    printf "%b\n" "\nInfo: Cleanup is running ..."
    # Additional cleanup tasks can be added here if needed
    rm -f -v ./*.tmp
    printf "%b\n" "\nInfo: Cleanup finished ..."
}

if [[ "$(uname)" == "Darwin" ]]; then
    HOMEDIR="Users"
    readonly HOMEDIR
elif [[ "$(uname)" == "Linux" ]]; then
    HOMEDIR="home"
    readonly HOMEDIR
else
    printf "Error: Unsupported operating system: %s\n" "$(uname)"
    exit 1
fi

# Under Linux you use `home` under macOS `Users`
echo "Home directory: ${HOMEDIR}"

USERSCRIPT="cloud"
# Your user! In which context it SHOULD run
echo "User script: ${USERSCRIPT}"

DIRDATE="$(date +"%Y-%m-%d")"
readonly DIRDATE
printf "%b\n" "\nCurrent date: ${DIRDATE}"

# Define constants
VERSION="0.5.3"
readonly VERSION

SCRIPT_NAME="$(basename "${0}")"
readonly SCRIPT_NAME

SCRIPT_PATH="$(realpath "$(dirname "${0}")")"
readonly SCRIPT_PATH

SCRIPT_PATH_WITH_NAME="${SCRIPT_PATH}/${SCRIPT_NAME}"
readonly SCRIPT_PATH_WITH_NAME

DIRECTORY="${SCRIPT_PATH}/tools"
printf "Info: ${DIRECTORY}"

DESTINATION="/${HOMEDIR}/${USERSCRIPT}/repos/"
echo "${DESTINATION}"

EXECUTIONPATH="${DESTINATION}"
echo "${EXECUTIONPATH}"

n="ndaal_customer_eswe_verkehr_feinkonzept"
echo "${n}"

Function_Git_Push_Repos () {
    echo " "
    echo "Git Push to Repos"
    echo " "
    git add --verbose -A || true
    git commit -m "update auditd best practices" || true
    #git push origin HEAD:master || true
    #git push --verbose --force || true
    #git push origin HEAD:main || true
    git push --verbose --force || true
}

Function_Create_Default_Information_for_Repos () {
    echo "update structure.txt"
    tree > ./"structure.txt" || exit
    cat ./"structure.txt" || exit
    echo "update content_summary.txt"
    tokei ---sort code > "content_summary.txt" || exit
    cat ./"content_summary.txt" || exit
    #echo "update content_long_list.txt"
    #tokei ---sort code --files > "content_long_list.txt" || exit
    #cat ./"content_long_list.txt" || exit
}

xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_auditd_public/ndaal" || true
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_auditd_public/dataset" || true

xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_public_auditd/ndaal" || true
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${EXECUTIONPATH}ndaal_public_auditd/dataset" || true

echo "doing some stuff for ${DESTINATION}${n}/documentation/"
echo "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_log/auditd_snippets_and_information.rst to ${DESTINATION}${n}/documentation/source/_log/" || exit
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_log/auditd_snippets_and_information.rst" "${DESTINATION}${n}/documentation/source/_log/" || exit
echo "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules to ${DESTINATION}${n}/documentation/source/_auditd/_audit.rules/" || exit
xcp -v "${EXECUTIONPATH}ndaal_ml_infra_deploy/documentation/source/_auditd/_audit.rules/audit_best_practices.rules" "${DESTINATION}${n}/documentation/source/_auditd/_audit.rules/" || exit

cp -p -f -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${SCRIPT_NAME}" "${EXECUTIONPATH}ndaal_auditd_public/tools"
cp -p -f -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${SCRIPT_NAME}" "${EXECUTIONPATH}ndaal_public_auditd/tools"
cp -p -f -v "/${HOMEDIR}/${USERSCRIPT}/repos/scripts/${SCRIPT_NAME}" "${DESTINATION}${n}/tools"

cd "${EXECUTIONPATH}ndaal_auditd_public/" || exit
echo "${EXECUTIONPATH}ndaal_auditd_public/"

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

cd "${EXECUTIONPATH}ndaal_public_auditd/" || exit
echo "${EXECUTIONPATH}ndaal_public_auditd/"

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

cd "${DESTINATION}${n}/" || exit
echo "${DESTINATION}${n}/"

Function_Create_Default_Information_for_Repos
Function_Git_Push_Repos

cd "${EXECUTIONPATH}ndaal_ml_infra_deploy/" || exit
echo "${EXECUTIONPATH}ndaal_ml_infra_deploy/"

#Function_Create_Default_Information_for_Repos
#Function_Git_Push_Repos

cleanup

script_name1="$(basename "${0}")"
printf "\nInfo: script_name1: %s\n" "${script_name1}"
script_path1="$(realpath "$(dirname "${0}")")"
printf "Info: script_path1: %s\n" "${script_path1}"
script_path_with_name="${script_path1}/${script_name1}"
printf "Info: Script path with name: %s\n" "${script_path_with_name}"
printf "Info: Script finished\n"
exit 0
